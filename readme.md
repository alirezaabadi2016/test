# H1 in markdown
## H2 in markdown
---
### H3 in markdown
---
#### H4 in markdown
---
##### H5 in markdown
---
###### H6 in markdown
---
**This is Strong text**
---
> this is quote


---
[this is a link to my git lab profile](https://gitlab.com/alirezaabadi2016)


---
## unorder list
<!-- ul -->
* c#
* python
* javascript
  * JQurey

* php
  * laravel
* C++
---
## order list
<!-- ol -->
1. a
2. b
3. c
4. d
---
## This is the logo of markdown
![this is a logo of markdown](https://miro.medium.com/max/2800/0*V9tvdEAZmSjBG3Ny.gif)

---
---
---
---

```javaScript
function sum(num1,num2){
    return num1 + num2;
}
```


```python
def sum(num1,num2)
    return num1 + num2
}
```

---
---
## table in markdown
| name    | Email  |
| ------- |------- |
| alireza | a@a    |
| alireza | A@A    |